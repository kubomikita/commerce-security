<?php


namespace Commerce;

use Kubomikita\Service;
use Nette\Database\Connection;
use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;
use Nette\Security\IIdentity;

class FrontendAuthenticator implements IAuthenticator {


	/**
	 * @param array $credentials
	 * Performs an authentication against e.g. database.
	 * and returns IIdentity on success or throws AuthenticationException
	 * @return IIdentity
	 * @throws AuthenticationException
	 */
	public function authenticate(array $credentials) : IIdentity
	{

		list($username, $password) = $credentials;
		$partner_id = $credentials[2] ?? null;
		//bdump($credentials);
		//exit;
		$db = Service::getByType(Connection::class);
		/*$qry = "select * from ".\Config::tablePartneriData."
            where username='".addslashes($username)."' and allow_login=1 and off<>1 and deny_login<>1";
		//echo $qry;
		$Q=mysqli_query(\CommerceDB::$DB,$qry);
		$r = mysqli_fetch_object($Q);
		//dump($r);*/
		$where = ["d.username" => $username];
		if($partner_id === null){
			$where["p.temporary"] = 0;
		}
		$r = $db->query("select d.* from ec_partneri_data as d LEFT JOIN ec_partneri as p ON p.id=d.partner_id  where",$where)->fetch(); // and d.allow_login=1 and d.off<>1 and d.deny_login<>1 and `temporary`=0
		//dumpe($r, $credentials);
		//bdump($r);
		if($r && $r->partner_id > 0){
			//bdump($r);
			switch($r->type){
				case null:
					$verified = hash_equals($r->password,md5($password));
					//dump($r->newpassword,md5("kiyeiokvdj"),$r);
					if(!$verified and $r->newpassword != "") {

						$verified=hash_equals($r->newpassword,md5($password));
						if($verified) {
							static::password_setnew($r->partner_id,$password);
						}
					}
					if($verified){
						static::password_rehash($username,$password);
					}
					break;
				case 1:
					$verified = password_verify(md5($password),$r->password);
					//if(!$verified) { $verified=password_verify(md5($password),$r->newpassword); static::password_setnew($r->partner_id,$r->newpassword);}
					if(!$verified and $r->newpassword != "") {
						$verified=hash_equals($r->newpassword,md5($password));
						if($verified) {
							static::password_setnew($r->partner_id,$password);
						}
					}
					if($verified){
						static::password_rehash($username,$password);
					}
					break;
				case 2:
					$verified = password_verify($password,$r->password);
					if(!$verified and $r->newpassword != "") {
						$verified=hash_equals($r->newpassword,md5($password));
						if($verified) {
							static::password_setnew( $r->partner_id, $password );
						}
					}
					break;
				default:
					$verified = false;
					break;
			}

			// BUGFIX: temporary nie
			$P=\Partner::getByUsername($username,$partner_id === null ? 0 : 1);
			//bdump($P);
			//exit;
			$verified = $verified ? (int)$r->partner_id : false;
			if($P->deny_login or $P->off or !$P->allow_login){
				$verified = false;
			} //and d.allow_login=1 and d.off<>1 and d.deny_login<>1
			//bdump($verified);
			//bdump($P);

			if(substr($password,0,5)=='@#$!!'){
				if($P->masterPassword()==$password){
					$verified = $P->id;

				};
			}
			if($verified){
				//
				//
				$P->visit();
				return new Identity($verified,null,$r);
			} else {
				throw new AuthenticationException("Nesprávne prihlasovacie meno alebo heslo.", self::INVALID_CREDENTIAL);
			}
		}

		throw new AuthenticationException("Nesprávne prihlasovacie meno alebo heslo.", self::IDENTITY_NOT_FOUND);
	}


	private static function password_rehash($user,$pass){
		$db = Service::getByType(Connection::class);
		$db->query('UPDATE '.\Config::tablePartneriData.' SET password = \''.password_hash($pass, PASSWORD_DEFAULT).'\' , type = 2 WHERE username = \''.$user.'\'');

	}
	private static function password_setnew($id,$pass){
		$db = Service::getByType(Connection::class);
		$new = password_hash($pass,PASSWORD_DEFAULT);
		$db->query('UPDATE '.\Config::tablePartneriData.' SET password = \''.$new.'\', newpassword=\'\' , type = 2 WHERE partner_id = \''.$id.'\'');
		bdump($id);
		bdump($pass);
	}
}
