<?php


namespace Commerce;

use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;
use Nette\Security\IIdentity;

class BackendAuthenticator implements IAuthenticator {
	private $db;
	public function __construct() {
		$this->db = \Registry::get("database");
	}

	/**
	 * @param array $credentials
	 * Performs an authentication against e.g. database.
	 * and returns IIdentity on success or throws AuthenticationException
	 * @return IIdentity
	 * @throws AuthenticationException
	 */
	public function authenticate(array $credentials) : IIdentity
	{
		list($username, $password) = $credentials;
		$q = $this->db->query("SELECT * FROM ".\Config::tableUsers." WHERE username=? and allow=1",$username);
		if($r = $q->fetch()){
			switch($r->type){
				case null:
					$verified = hash_equals($r->pwd,md5($password));
					if($verified){
						static::password_rehash($username,$password);
					}
					break;
				case 1:
					$verified = password_verify(md5($password),$r->pwd);
					if($verified){
						static::password_rehash($username,$password);
					}
					break;
				case 2:
					$verified = password_verify($password,$r->pwd);
					break;
				default:
					$verified = false;
					break;
			}
			$verified = $verified ? (int)$r->id : false;

			if($verified){
				unset($r->pwd);
				unset($r->type);
				$r->metadata = unserialize($r->metadata);
				$r->objednavka = unserialize($r->objednavka);

				return new Identity($verified,null,$r);
			} else {
				throw new AuthenticationException("Invalid credentials", self::INVALID_CREDENTIAL);
			}
		}

		throw new AuthenticationException("User '$username' not found.", self::IDENTITY_NOT_FOUND);
	}


	public function password_rehash($user,$pass){
		$this->db->query('UPDATE '.\Config::tableUsers.' SET pwd = ? , type = 2 WHERE username = ?',password_hash($pass, PASSWORD_DEFAULT),$user);
	}
}
